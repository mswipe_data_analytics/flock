# List of Hot words: #
(* these hot words have a yesterday version e.g. for tpv? the yesterday version in tpv yesterday?)

bin XXXXXX: pull out the resp_code_desc and count of trxns for the bin XXXXXX in the last 30 mins

max tpv? : highest single approved transaction for the day *

qr? : Total of jv_amount for jv.createdby = mQR of UPI *

tps? : count of transactions in the last 30 mins / (60*30) 

bin? : should result in the bin report that comes every 30 mins

wiki topic : to read about your fevorit topic in Wikipedia

pending appl? : count of pending applications *

install? : count of instalations done today  *

zone install? : count of instalations done today with zone break up. *

razorpay? : Total of transaction amount for Razorpay for the day *

active razorpay? : Total of active terminals for Razorpay for the day

tpv? : Todays transaction volume *

top tpv? : Todays top 5 highest approved transaction for the day *

top tpv cr? : Todays top 5 highest approved transaction for the day (corporate) *

tpv hourly? : Last hours transaction split on CR and RE

top tpv qr? : Todays top 5 highest approved qr transaction for the day *

zone tpv? : Zone wise approved transaction for the day *

zone tpv RE? : Zone wise approved transaction RE for the day  *

zone tpv CR? : Zone wise approved transaction CR for the day *

qr zone tpv? : Zone wise approved transaction for the day for QR only *

qr install? : Count of QR installs for today *

repeat caller? : Top 5 repeat callers (7days) *

active crm? : Todays active CRM *

active fse? : Todays active FSE *

image xxxxxxxxxx? : Images of the customer

application xxxxxxxxxx? : Application of the customer

tpv RE? : Retail TPV for the day *

failure? : Top 10 failure discriptions for the day

update? : latest time stamp in LJV and some other tables

top city tpv? : Top city(5) wise approved transaction for the day *

active terminal? : Count of active terminals for the *

pending tickets? : Details of pending tickets

Cust Device id xxxxxxxxxxxxxx? : Details of the device

top total cr? The top 5 (CR) customers by volume for the day *

top total re? The top 5 (RE) customers by volume for the day *

Churn report? Red to green and greeen movements of MEs

re mtd tpv? retail mtd tpv, target and daily no. needed to achive it

cohort tpv? Yearly cohort based tpv change %

vivo? vivo T and T-1 transaction trend

vivo trend? vivo 10 days transaction trend
